#define VOICE_INTERFACE "VOICE_INTERFACE_002"

class IVoiceListener
{
public:
	// In raw mode, nSamples = bytes;
	virtual void OnVoiceBroadcast(int client, char *pVoiceData, int *nSamples)
	{
	}
};

class IVoice
{
public:
	virtual void AddVoiceListener(IVoiceListener *pVoiceListener) = 0;
	virtual void RemoveVoiceListener(IVoiceListener *pVoiceListener) = 0;
	
	virtual void AddRawVoiceListener(IVoiceListener *pVoiceListener) = 0;
	virtual void RemoveRawVoiceListener(IVoiceListener *pVoiceListener) = 0;
};