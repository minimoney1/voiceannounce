/*
 * ================================================================================
 * VoiceAnnounce
 * Copyright (C) 2011 Asher "asherkin" Baker.  All rights reserved.
 * ================================================================================
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "extension.h"

VoiceAnnounce g_VoiceAnnounce;

SMEXT_LINK(&g_VoiceAnnounce);

IVoice *g_pVoice = NULL;
IChangeableForward *g_pClientSpeakingFwd = NULL;

cell_t AddVoiceHook(IPluginContext *pContext, const cell_t *params)
{
	g_pClientSpeakingFwd->AddFunction(pContext, static_cast<funcid_t>(params[1]));
	return 1;
}
 
cell_t RemoveVoiceHook(IPluginContext *pContext, const cell_t *params)
{
	IPluginFunction *pFunction = pContext->GetFunctionById(static_cast<funcid_t>(params[1]));
	g_pClientSpeakingFwd->RemoveFunction(pFunction);
	return 1;
}
const sp_nativeinfo_t g_Natives[] = 
{
	{"AddVoiceHook",             AddVoiceHook},
	{"RemoveVoiceHook",			 RemoveVoiceHook},
	{NULL,							NULL},
};

void VoiceAnnounce::OnVoiceBroadcast(int client, char *pVoiceData, int *nSamples)
{
	g_pClientSpeakingFwd->PushCell(client);
	g_pClientSpeakingFwd->Execute(NULL);
}

bool VoiceAnnounce::SDK_OnLoad(char *error, size_t maxlen, bool late)
{
	g_pClientSpeakingFwd = forwards->CreateForwardEx(NULL, ET_Ignore, 1, NULL, Param_Cell);
	sharesys->AddNatives(myself, g_Natives);
	sharesys->RegisterLibrary(myself, "voiceannounce");
	return true;
}

void VoiceAnnounce::SDK_OnUnload()
{
	forwards->ReleaseForward(g_pClientSpeakingFwd);
}

bool VoiceAnnounce::SDK_OnMetamodLoad(ISmmAPI *ismm, char *error, size_t maxlen, bool late)
{
	int ifaceerror;
	g_pVoice = (IVoice *)g_SMAPI->MetaFactory(VOICE_INTERFACE, &ifaceerror, NULL);
	if (!g_pVoice)
	{
		snprintf(error, maxlen, "Could not find interface %s (error %d)", VOICE_INTERFACE, ifaceerror);
		return false;
	}

	g_pVoice->AddRawVoiceListener(this);

	return true;
}

bool VoiceAnnounce::SDK_OnMetamodUnload(char *error, size_t maxlen)
{
	g_pVoice->RemoveRawVoiceListener(this);

	return true;
}